#!/bin/sh

FORECAST_DAYS=5

CURRENT_URL="https://api.weatherapi.com/v1/current.json?key=$WEATHER_API_KEY&q=$HOME_LON_LAT&aqi=no"
FORECAST_URL="https://api.weatherapi.com/v1/forecast.json?key=$WEATHER_API_KEY&q=$HOME_LON_LAT&days=$FORECAST_DAYS&aqi=no&alerts=yes"

RESPONSE="$(curl -s -X GET $FORECAST_URL | jq -f weather_api_current_jq_filter.txt)"

TIMESTAMP=$(echo $RESPONSE | jq -r .timestamp)
DESCRIPTION=$(echo $RESPONSE | jq -r .description)
TEMP_C=$(echo $RESPONSE | jq -r .temp_c)
TEMP_FEELS_LIKE_C=$(echo $RESPONSE | jq -r .feels_like_c)
WIND_MPH=$(echo $RESPONSE | jq -r .wind_mph)
WIND_DIR=$(echo $RESPONSE | jq -r .wind_dir)
PRECIPITATION_MM=$(echo $RESPONSE | jq -r .precipitation_mm)

CURRENT="$TIMESTAMP $DESCRIPTION $TEMP_C°C($TEMP_FEELS_LIKE_C°C) ${WIND_MPH}mph(${WIND_DIR}) ${PRECIPITATION_MM}mm"

echo $CURRENT
