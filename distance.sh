#!/bin/sh

HERE="$(lat_lng.sh)"
THERE="$1"
ROUNDING=$2

python3 haversine.py "$HERE" "$THERE" $ROUNDING
