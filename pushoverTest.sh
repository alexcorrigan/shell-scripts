#!/bin/bash

APP_KEY="token=$1"
USER_KEY="user=$2"
MSG=$3

curl -s -F "$APP_KEY" -F "$USER_KEY" -F "$MSG" https://api.pushover.net/1/messages.json
