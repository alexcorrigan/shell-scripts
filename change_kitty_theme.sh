#!/bin/bash

KITTY_CONF_DIR="${HOME}/.config/kitty"
KITTY_THEMES_DIR="$KITTY_CONF_DIR/themes"
KITTY_THEME_CONF_FILE="$KITTY_CONF_DIR/theme.conf"

SELECTED_THEME=$(ls ${KITTY_THEMES_DIR} | cut -d '.' -f 1 | fzf)

if [ -z $SELECTED_THEME ]; then
    echo "No theme selected."
    exit 0
else
    echo "Set kitty theme: $SELECTED_THEME"
    echo "include ${KITTY_THEMES_DIR}/${SELECTED_THEME}.conf" > $KITTY_THEME_CONF_FILE
fi
