#!/bin/sh

PUB_IP=$(public_ip.sh)
LOCATION=$(curl -s http://ip-api.com/json/$PUB_IP)

LAT=$(echo $LOCATION | jq .lat -r)
LNG=$(echo $LOCATION | jq .lon -r)

echo "{\"latitude\": $LAT, \"longitude\": $LNG}"
