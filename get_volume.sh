#!/bin/sh

MUTED=$(amixer sget Master | grep 'Mono:' | awk -F'[][]' '{ print $6 }')
VOLUME=$(amixer sget Master | grep 'Mono:' | awk -F'[][]' '{ print $2 }')

if [ $MUTED == off ]
then
	echo "MUTE"
else
	echo $VOLUME
fi

