import sys
import math
import json
from dataclasses import dataclass

MEAN_EARTH_RADIUS_KM = 6371


@dataclass
class Place:
    latitude: float
    longitude: float

    @staticmethod
    def from_dict(coordinates: dict):
        return Place(coordinates['latitude'], coordinates['longitude'])


def haversine(from_place: Place, to_place: Place) -> float:
    delta_lat = (from_place.latitude - to_place.latitude) * math.pi / 180
    delta_long = (from_place.longitude - to_place.longitude) * math.pi / 180

    to_lat_rads = to_place.latitude * math.pi / 180
    from_lat_rads = from_place.latitude * math.pi / 180

    a = math.pow(math.sin(delta_lat / 2), 2) + math.pow(math.sin(delta_long / 2), 2) * math.cos(from_lat_rads) * math.cos(to_lat_rads)
    c = 2 * math.asin(math.sqrt(a))
    distance_km = MEAN_EARTH_RADIUS_KM * c
    distance_miles = distance_km * 0.621371
    return distance_miles


if __name__ == "__main__":
    place_a = Place.from_dict(json.loads(sys.argv[1]))
    place_b = Place.from_dict(json.loads(sys.argv[2]))
    rounding = int(sys.argv[3])
    distance = round(haversine(place_a, place_b), rounding)
    print(distance)
