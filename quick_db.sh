#!/bin/sh

docker run -d \
    --name some-postgres \
    -e POSTGRES_PASSWORD=password \
	-p 5432:5432 \
    postgres
