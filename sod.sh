#!/bin/sh

#nohup firefox &
#sleep 1
#
#nohup urxvt -e tmux &
#sleep 1
#
#nohup pycharm.sh &
#sleep 1
#
#nohup idea.sh &
#sleep 1
#
#nohup datagrip.sh &
#sleep 1
#
#nohup insomnia &

wmctrl -s 0
kitty &

sleep 1

wmctrl -s 1
idea.sh &

sleep 8

wmctrl -s 2
firefox --new-window "https://origamienergy.atlassian.net/jira/software/c/projects/EDP/boards/209" &
sleep 1
firefox --new-tab "https://gitlab.com/OrigamiEnergyLtd/energy-data-platform" &

sleep 1

wmctrl -s 3
firefox --new-window "https://outlook.office.com/calendar/view/week" &

slack &

sleep 2


