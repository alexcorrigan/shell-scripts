#!/bin/bash

BACK_TITLE="back title"
TITLE="title"
MENU="choose something"

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4

OPTIONS=(
	1 "A"
	2 "B"
	3 "C")

CHOICE=$(dialog --clear --backtitle "$BACK_TITLE" --title "$TITLE" --menu "$MENU" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)


clear

case $CHOICE in
	1)
		echo "A"
		;;
	2)
		echo "B"
		;;
	3)
		echo "C"
		;;
esac
