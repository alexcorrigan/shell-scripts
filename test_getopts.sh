#!/bin/bash


while getopts "bh:" OPT; do
    case $OPT in
        b)
            echo "Will build" >&2
            ;;
        h)
            echo "Container host = $OPTARG" >&2
            ;;
        *)
            echo "Invalid! ... $OPTARG"
            ;;
    esac
done

