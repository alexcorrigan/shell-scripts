#!/bin/bash

MOUSE="Razer Mamba Elite"
MOUSE_MAT="Razer Goliathus"

RED="#ff0000"

GREEN="#00ff00"
ARCTIC_LIME="d0ff14"

BLUE="#0000ff"
BRIGHT_TURQUIOSE="#08e8de"

#polychromatic-cli -n "$MOUSE" -z logo -o spectrum
#polychromatic-cli -n "$MOUSE" -z logo -o breath -p single -c $RED
#polychromatic-cli -n "$MOUSE" -o breath -p single -c $BLUE
#polychromatic-cli -n "$MOUSE" -o breath -p dual -c $GREEN,$RED

#polychromatic-cli -n "$MOUSE" -o none -p down
#polychromatic-cli -n "$MOUSE" -z logo -o wave -p down

#polychromatic-cli -n "$MOUSE" -o wave -p down

# DUAL STATIC COLOUR
#
STATIC_COLOUR=$ARCTIC_LIME
polychromatic-cli -n "$MOUSE" -o static -c $STATIC_COLOUR
polychromatic-cli -n "$MOUSE_MAT" -o static -c $STATIC_COLOUR

