#!/bin/sh

CPU_TEMP="$(sensors | grep -i 'package id 0' | awk '{print $4}')"
CPU_FAN="$(sensors | grep -i 'fan1' | awk '{print $2 RPM}') RPM"

echo "${CPU_TEMP} ${CPU_FAN}"
