#!/bin/sh

TRACK_INFO=$(mpc status -f "[%artist% - %title% - %album%]" | head -1)
PLAY_STATUS=$(mpc status | tail -2 | head -1 | cut -d '[' -f2 | cut -d ']' -f1)
PROGRESS_PERCENT=$(mpc status | tail -2 | head -1 | cut -d '(' -f2 | cut -d ')' -f1)

echo "[$PLAY_STATUS - $PROGRESS_PERCENT] $TRACK_INFO"
