#!/bin/bash

# find connected connected monitors
#xrandr | grep " connected"


if [ $HOSTNAME == "origami" ] && [ $(xrandr | grep " connected" | grep "DP-1-3" | grep "primary" | wc -l) == 1 ]
then
	#turn off laptop monitor
	xrandr --output eDP-1-1 --off
else
	echo "use laptop"
fi

