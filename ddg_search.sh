#!/bin/sh

TERM=$1

urlencode() {
	echo ${TERM// /"%20"}
}

LANG="uk-en"
#URL="https://www.duckduckgo.com/?q="$TERM")&type=list&kl=$LANG"
#URL="https://duckduckgo.com/?q=$TERM"
URL="https://duckduckgo.com/?q=$TERM&kv=1&ko=-2&k1=-1&kz=-1&kae=d&kav=-1"

curl -X GET "$URL" > ~/tmp/searchResults.html

brave-browser $URL
