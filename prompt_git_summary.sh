#!/bin/sh

REPO=$(git_repo.sh)
BRANCH=$(git branch --show-current)
CHANGES=$(git_uncommited_count.sh)
COMMITS=$(git_commits_not_pushed.sh)
STASHED=$(git_stashed.sh)

ICON_PROMPT=true

if $ICON_PROMPT
then
    SYMBOL_GIT=$(git_service_icon.sh)
    SYMBOL_BRANCH=" "
    SYMBOL_REPO=" "
    SYMBOL_CHANGES=" "
    SYMBOL_COMMITS=" "
    SYMBOL_STASHED=" "  
else
    SYMBOL_GIT="GIT"
    SYMBOL_BRANCH=""
    SYMBOL_REPO=""
    SYMBOL_CHANGES="Ch."
    SYMBOL_COMMITS="Cm."
    SYMBOL_STASHED="St."  
fi

SUMMARY="${FG_LYELLOW}[${SYMBOL_GIT}${FG_DARKGRAY} ${SYMBOL_REPO}${REPO} ${SYMBOL_BRANCH}${BRANCH} (${SYMBOL_CHANGES}${CHANGES})(${SYMBOL_COMMITS}${COMMITS})(${SYMBOL_STASHED}${STASHED})${FG_LYELLOW}]${FG_DEFAULT}"

echo $SUMMARY
