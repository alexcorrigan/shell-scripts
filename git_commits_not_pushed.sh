#!/bin/sh

CURRENT_BRANCH=$(git branch --show-current)
REMOTES=$(git branch --all | grep "remotes/origin/${CURRENT_BRANCH}" | wc -l)

if [ $REMOTES -gt 0 ]
then
	COMMITS=$(git cherry | wc -l)
	echo $COMMITS
else
	echo 0
fi
