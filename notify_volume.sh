#!/bin/sh

MUTED=$(amixer sget Master | grep 'Mono:' | awk -F'[][]' '{ print $6 }')
VOLUME=$(amixer sget Master | grep 'Mono:' | awk -F'[][]' '{ print $2 }')

function notify {
	notify-send -t 1000 -i ~/.config/icons/volume.png $1
}

if [ $MUTED == off ]
then
	notify "MUTED"
else
	notify $VOLUME
fi

