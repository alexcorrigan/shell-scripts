#!/usr/bin/python

import time
from yeelight import *
from yeelight.transitions import *
from yeelight import Flow

living_rm_small = Bulb("192.168.0.38")
living_rm_tall = Bulb("192.168.0.39")
#orb = Bulb("192.168.0.40", effect="smooth", duration=10000)
office = Bulb("192.168.0.41")
strip = Bulb("192.168.0.42")

def toggle(bulb):
	bulb.toggle()

def props(bulb):
	return bulb.get_properties()

def brightness(bulb, brightness):
	bulb.set_brightness(brightness)

def colour(bulb, red, green, blue):
	bulb.set_rgb(red, green, blue)
	
#colour(office, 100, 50, 0)
#brightness(office, 10)
#
#print(props(office))

flow = Flow(count=10, transistions=lsd(3000, 10)
office.start_flow(flow)
