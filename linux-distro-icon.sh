#!/bin/bash

DISTRO_UNKNOWN=""
DISTRO_ARCH=""
DISTRO_UBUNTU=""

DISTRO="$(grep -Eh "^NAME=" /etc/os-release | cut -d '=' -f 2 | tr -d '"')"

if [ $DISTRO = "Ubuntu" ]; then
    echo "$DISTRO_UBUNTU "
elif [ $DISTRO = "Arch Linux" ]; then
    echo "$DISTRO_ARCH "
else
    echo "$DISTRO_UNKNOWN "
fi
