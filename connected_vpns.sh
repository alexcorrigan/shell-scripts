#!/bin/bash

CONNECTED_VPNS=$(nmcli con | grep -i vpn | grep -v -- '--' | awk -F ' ' '{ print $1 }')

if [ -z $CONNECTED_VPNS ]
then
	echo "none"
else
	echo $CONNECTED_VPNS
fi

