#!/bin/sh

PY_VERSION=$(python --version | cut -d " " -f 2)

SUMMARY="${FG_LGREEN}[ ${FG_DARKGRAY}${PY_VERSION} $(X=${VIRTUAL_ENV%/*/*}; echo ${VIRTUAL_ENV#"$X"})${FG_LGREEN}]${FG_DEFAULT}"
#SUMMARY="${FG_LGREEN}[PY ${FG_DARKGRAY}$(X=${VIRTUAL_ENV%/*/*}; echo ${VIRTUAL_ENV#"$X"})${FG_LGREEN}]${FG_DEFAULT}"

echo $SUMMARY
