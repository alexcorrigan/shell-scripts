#!/bin/bash

SAT_NAME=$1

DELAY_SECS=15 # 240 requests / hour
ROUNDING=2

PREVIOUS_DISTANCE=0

while true; do
	TIME=$(date +"%H:%M:%S")
	SAT_LOCATION=$(satelite_location.sh $SAT_NAME)
	DISTANCE=$(distance.sh "$SAT_LOCATION" $ROUNDING)
	CHANGE=$(echo "$DISTANCE - $PREVIOUS_DISTANCE" | bc -l)
	echo "🛰️ $TIME -- $SAT_NAME is $DISTANCE miles away (Δ$CHANGE)"
	PREVIOUS_DISTANCE=$DISTANCE
	sleep $DELAY_SECS
done
