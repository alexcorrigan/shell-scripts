#!/bin/bash

#SPARK_APPS=$(kubectl -n edp-commercial-management get sparkapplications | tail -n+2 | awk -F ' ' '{print $1}')
SPARK_APPS=$(kubectl -n edp-commercial-management get sparkapplications | tail -n+2 | cut -d ' ' -f 1)

for APP in $SPARK_APPS; do
	echo $APP
done
