#!/bin/sh

DISTANCE=2
DELAY=60

while true; do
	echo -e "$(date) - ${FG_CYAN}jiggled${FG_DEFAULT}"
	xdotool mousemove_relative $DISTANCE $DISTANCE
	sleep 0.5
	xdotool mousemove_relative -- -$DISTANCE -$DISTANCE
	sleep $DELAY
done
