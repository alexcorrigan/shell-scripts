#!/bin/bash 

VAULT_LOCATION=~/vault

SOURCE=~/vault/

LOCAL_BACK_UP=~/Desktop/vault_local_backup

MNEMOSYNE=/media/alex/MNEMOSYNE/vault

TIMESTAMP=$(TZ=UTC0 date +"%Y-%m-%dT%H:%M:%S%z")
LOCAL_TIME=$(date +"%H:%M:%S (%Z)")

# --

# --

echo "Backing up to local..."
rsync -avhtP $SOURCE $LOCAL_BACK_UP
echo "$TIMESTAMP .. $LOCAL_TIME .. Local back-up " >> ~/vaultBackUp.log


# -a = archive: syncs recursively and preserves symbolic links, special and device files, modification times, groups, owners, and permissions
# -v = verbose
# -h = human readable: numbers in human readable format
# -P = combines the flags --progress and --partial: progress bar and resume interrupted transfers
