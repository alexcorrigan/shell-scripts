#!/bin/bash

BACK_TITLE="back title"
TITLE="title"
MENU="choose something"

HEIGHT=3
WIDTH=40
CHOICE_HEIGHT=4

CHOICE=$(dialog --clear --backtitle "$BACK_TITLE" --title "$TITLE" --calendar "Choose a date:" $HEIGHT $WIDTH 26 1 2020 2>&1 >/dev/tty)

clear
echo $CHOICE
