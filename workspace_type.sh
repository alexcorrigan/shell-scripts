#!/bin/bash

CURR_PATH=$(pwd)

WORKSPACE_PATH=$(echo $CURR_PATH | grep -Eo "$HOME/workspace/(work|personal)")

if [ ! -z $WORKSPACE_PATH ]
then
    WORKSPACE_TYPE=$(basename $WORKSPACE_PATH)
else
    WORKSPACE_TYPE=""
fi

echo $WORKSPACE_TYPE
