#!/bin/bash

CURR_PATH=$(pwd)

WORKSPACE_PATH=$(echo $CURR_PATH | grep -Eo "$HOME/workspace/(work|personal)")

if [ ! -z $WORKSPACE_PATH ]
then
    TMP=${CURR_PATH%/*/*}
    PROMPT_PATH="${CURR_PATH#"$WORKSPACE_PATH"}"
else
    PROMPT_PATH=""
fi

echo $PROMPT_PATH
