#!/bin/sh

# normal repo
if git rev-parse --is-inside-work-tree > /dev/null 2>&1; then
    REPO_TOP_LEVEL=$(git rev-parse --show-toplevel)
    echo $(basename $REPO_TOP_LEVEL)
fi
