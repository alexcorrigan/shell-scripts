#!/bin/sh

ADDED=$(git status --porcelain | grep -E '^\s?A' | wc -l)
UNTRACKED=$(git status --porcelain | grep -E '^(\?\?|\s?M)' | wc -l)

echo $(($ADDED + $UNTRACKED))
