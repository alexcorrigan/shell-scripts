#!/bin/bash

ICON_GITLAB=""
ICON_GITHUB=""
ICON_GIT=""

function isGitService {
    local SERVICE_NAME=$1
    git config --get remote.origin.url | grep -o $SERVICE_NAME
}

if [ $(isGitService gitlab) ]; then
    echo "$ICON_GITLAB "
elif [ $(isGitService github) ]; then
    echo "$ICON_GITHUB "
else
    echo "$ICON_GIT "
fi
