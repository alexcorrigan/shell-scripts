#!/bin/sh

LOCATION="Cambridge"
URL_BASE="wttr.in/${LOCATION}"

CURRENT_TIME="$(date +%k%M)"

get () {
	FORMAT=$1
	URL="${URL_BASE}?format=${FORMAT}"
	echo "$(curl -s $URL)"
}

TOD="Day"

CONDITION=$(get "%c")
MOON_PHASE=$(get "%m")

SUNRISE=$(date -d "$(get "%S")" +%k%M)
SUNSET=$(date -d "$(get "%s")" +%k%M)

SUMMARY="$MOON_PHASE"

echo $SUNRISE $CURRENT_TIME $SUNSET

if [ $CURRENT_TIME -gt $SUNRISE -a $CURRENT_TIME -lt $SUNSET ]; then
	# IT'S DAY
	A=1
else
	# IT'S NIGHT
	if [ $CONDITION = "☀️" ]; then
		TOD="Night"
		#CONDITION="$MOON_PHASE"
	fi
fi

echo $TOD $CONDITION
