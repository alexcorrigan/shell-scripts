#!/bin/sh

PUB_IP=$(public_ip.sh)
LOCATION=$(curl -s http://ip-api.com/json/$PUB_IP)

LAT=$(echo $LOCATION | jq .lat -r)
LNG=$(echo $LOCATION | jq .lon -r)

TODAY=$(date +%Y-%m-%d)


RESPONSE=$(curl -s "https://api.sunrise-sunset.org/json?lat=${LAT}&lng=${LNG}&date=${TODAY}" | jq .)

SUNRISE=$(echo $RESPONSE | jq .results.sunrise -r)
NOON=$(echo $RESPONSE | jq .results.solar_noon -r)
SUNSET=$(echo $RESPONSE | jq .results.sunset -r)

echo "$SUNRISE - $NOON - $SUNSET"
