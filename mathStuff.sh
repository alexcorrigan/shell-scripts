#!/bin/bash

MID_WIDTH=$(echo "scale=0; $(tput cols) / 2" | bc)
MID_HEIGHT=$(echo "scale=0; $(tput lines) / 2" | bc)

echo $MID_WIDTH
echo $MID_HEIGHT
