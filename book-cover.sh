#!/bin/sh

ISBN=$1

firefox "$(curl -s -X GET "https://www.googleapis.com/books/v1/volumes?q=isbn:${ISBN}" | jq -r .items[0].volumeInfo.imageLinks.thumbnail)"
