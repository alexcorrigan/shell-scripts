#!/bin/sh

MINUTES=$1
MESSAGE=$2

SECONDS=$(($MINUTES*60))

sleep $SECONDS

notify-send "$MESSAGE"

