#!/bin/sh



LOCAL=$(date +"%H:%M")
UTC=$(TZ=UTC date +"UTC %H:%M")

echo "$LOCAL ($UTC)"
