#!/bin/sh

SECONDS_TO_WAIT_BETWEEN_SYNCS=$1

while true
do
	echo "- Running mail sync @ $(date)"
	mail_sync
	notify-send "Mail Sync'd"
	sleep $SECONDS_TO_WAIT_BETWEEN_SYNCS
done
