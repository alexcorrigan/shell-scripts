#!/bin/bash

SAT_NAME=$1

ISS_NORAD_ID=25544
TIANGONG_NORAD_ID=48274
JAMES_WEBB_SPACE_TELESCOPE=50463

if [ $SAT_NAME == "iss" ]
then
	SATELITE_NORAD_ID=$ISS_NORAD_ID
elif [ $SAT_NAME == "tiangong" ]; then
	SATELITE_NORAD_ID=$TIANGONG_NORAD_ID
elif [ $SAT_NAME == "jwst" ]; then
	SATELITE_NORAD_ID=$JAMES_WEBB_SPACE_TELESCOPE
else
	echo "Don't know that satelite!"
	exit 1
fi

LOCAL_LAT_LONG=$(lat_lng.sh)

LATITUDE=$(echo $LOCAL_LAT_LONG | jq -r .lat)
LONGITUDE=$(echo $LOCAL_LAT_LONG | jq -r .lng)

ALTITUDE=0

SECONDS_FROM_NOW=1

SATELITE=$(curl -s https://api.n2yo.com/rest/v1/satellite/positions/$SATELITE_NORAD_ID/$LATITUDE/$LONGITUDE/$ALTITUDE/$SECONDS_FROM_NOW?apiKey=$N2YO_API_KEY)

SAT_LAT=$(echo $SATELITE | jq .positions[0].satlatitude)
SAT_LNG=$(echo $SATELITE | jq .positions[0].satlongitude)

#echo $(echo $SATELITE | jq .positions[0])
echo "{\"latitude\": $SAT_LAT, \"longitude\": $SAT_LNG}"
