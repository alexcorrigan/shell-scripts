#!/bin/bash

AC_HOME="/sys/class/power_supply/AC"
BATTERY_HOME="/sys/class/power_supply/BAT0"

STATUS=""

AC_ONLINE="$(cat $AC_HOME/online)"


BATTERY_CAPACITY="$(( $(cat $BATTERY_HOME/capacity) ))%"
CHARGING_STATUS="$(cat $BATTERY_HOME/status)"

# Charging, Discharging, ???

#echo "B: ${BATTERY_CAPACITY}% - $CHARGING_STATUS"

if [ $CHARGING_STATUS == "Discharging" ]
then
	CHARGING_STATUS_CHAR=" ▼"
elif [ $CHARGING_STATUS == "Charging" ]
then
	CHARGING_STATUS_CHAR=" ▲"
else
	CHARGING_STATUS_CHAR=""
fi


STATUS="$STATUS$BATTERY_CAPACITY$CHARGING_STATUS_CHAR"



if [ $AC_ONLINE == "1" ]
then
	STATUS="$STATUS AC"
fi

echo -e "$STATUS"
